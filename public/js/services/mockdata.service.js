angular.module('MockService', [])
        .factory('MockDataService', ['$http', function ($http) {

                return {
                    getdata: function () {
                        var mockdata = [
                            {'username': "Denny", 'text': "Hi", 'date': new Date()},
                            {'username': "Denny", 'text': "Hello", 'date': new Date()},
                            {'username': "Denny", 'text': "How Are you?", 'date': new Date()},
                            {'username': "Denny", 'text': "Fine!", 'date': new Date()},
                            {'username': "Denny", 'text': "What are you doing?", 'date': new Date()},
                            {'username': "Denny", 'text': "Hangout.", 'date': new Date()},
                            {'username': "Denny", 'text': "Let's go", 'date': new Date()},
                            {'username': "Denny", 'text': "Willson", 'date': new Date()},
                            {'username': "Denny", 'text': "Good Morning", 'date': new Date()},
                            {'username': "Denny", 'text': "Its Nice", 'date': new Date()},
                            {'username': "Denny", 'text': "Okay", 'date': new Date()}
                        ];

                        var temp = mockdata[Math.floor(Math.random() * mockdata.length)];
                        return temp;
                    }
                };
            }]);