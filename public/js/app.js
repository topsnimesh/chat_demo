angular.module('chatApplication', [
    'ui.router',
    'ui.router.state.events',
    'ngMessages',
    'ngToast',
    'ngSanitize',
    'ngStorage',
    'luegg.directives',
    'appRoutes',
    'appRun',
    'MainCtrl',
    'LoginCtrl',
    'MockService'
]);

