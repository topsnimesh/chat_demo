// public/js/appRoutes.js
angular.module('appRoutes', []).config(['$stateProvider', '$urlRouterProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $locationProvider) {

        $stateProvider
                .state('login', {
                    url: '/login',
                    templateUrl: 'views/login.html',
                    controller: 'LoginController',
                    resolve: {
                        currentUser: ['$localStorage', '$state', function ($localStorage, $state) {
                                if ($localStorage.user_detail) {
                                    $state.go('main', {}, {location: 'replace'});
                                }
                            }]
                    }
                })
                .state('main', {
                    url: '/chat',
                    templateUrl: 'views/main.html',
                    controller: 'MainController',
                    resolve: {
                        currentUser: ['$localStorage', '$state', function ($localStorage, $state) {
                                if ($localStorage.user_detail === undefined) {
                                    $state.go('login', {}, {location: 'replace'});
                                }
                            }]
                    }
                });

        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/login');

    }]);

angular.module('appRun', [])
        .run(['$rootScope', '$localStorage', '$state',
            function ($rootScope, $localStorage, $state) {
                if ($localStorage.user_detail) {
                    $rootScope.user_detail = $localStorage.user_detail;
                }
            }]);

