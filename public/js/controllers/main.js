// public/js/controllers/MainCtrl.js
angular.module('MainCtrl', [])
        .controller('MainController', function ($scope, $localStorage, $rootScope, MockDataService, $timeout, $state, $location, $document) {

            $scope.chatMessages = [];
            var tone = new Audio('./tone/gets-in-the-way.mp3');
            $scope.glued = true;
            $scope.postMessage = function () {
                if ($scope.message && $scope.message != "") {
                    var temp_text = {
                        'username': $rootScope.user_detail.username,
                        'text': $scope.message,
                        'date': new Date()
                    };
                    $scope.chatMessages.push(temp_text);
                    $scope.message = "";
                    $timeout(function () {
                        var reply = MockDataService.getdata();
                        $scope.chatMessages.push(reply);
                        tone.play();
                    }, 1000);
                }
            };
            
            $scope.logout = function () {
                delete $localStorage.user_detail;
                delete $rootScope.user_detail;
                $state.go('login');
            };
        });

