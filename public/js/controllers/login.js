// public/js/controllers/MainCtrl.js
angular.module('LoginCtrl', [])
        .controller('LoginController', function ($scope, $state, ngToast, $localStorage, $rootScope) {

            $scope.loginUser = function () {
                if ($scope.user.username == 'Nilesh' && $scope.user.password == '123123') {
                    $localStorage.user_detail = $scope.user;
                    $rootScope.user_detail = $localStorage.user_detail;
                    $state.go('main');
                    ngToast.dismiss();
                    ngToast.create({
                        content: 'Login Success',
                        dismissOnClick: 'true'
                    });
                } else {
                    ngToast.dismiss();
                    ngToast.create({
                        className: 'danger',
                        content: 'Invalid Username or Password!',
                        dismissOnClick: 'true'
                    });
                }
            };
        });